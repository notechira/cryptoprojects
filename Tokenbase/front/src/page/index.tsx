import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Form, Row, Col, Button, Toast } from 'react-bootstrap'
import { useForm } from "react-hook-form";
import axios from 'axios'
import moment from 'moment'
import jwt from 'jwt-decode'

export default function TokenPage(){
    const { 
        register, 
        handleSubmit,
     } = useForm();

    const [showJwt, setShowJwt] = useState(false);
    const [showUser, setShowUser] = useState(false);

    const [JWTToken, setJWT] = useState<string>('');
    const [JWTDecode, setJWTDecode] = useState<any>(null);
    const [userData, setuserData] = useState<any>(null);
    const [RefreshToken, setRefresh] = useState<string>('');
    const [time, setTimes] = useState<any>(null)

    const [isLogin, setIsLogin] = useState(false);

    useEffect(() => {
        const interval = setInterval(() => {
            if(JWTDecode){
                if(moment(new Date(1000*JWTDecode.exp)).diff(moment(), 's') < 0) {
                    setTimes('Token expired')
                    
                } else setTimes( `${moment(new Date(1000*JWTDecode.exp)).diff(moment(), 's')} seconds` )
                
            }
            else setTimes('')
        }, 1000);
        return () => clearInterval(interval);
      });

    const handleonSubmit = ( data: any ) => {
        console.log(data)
        if(JWTToken === ''){
        axios.post(`http://localhost:5000/auth/login`, { 
            "username" : data.username, 
            "password" : data.password
        })
        .then(res => {
          setJWT(res.data.access_token)
          setRefresh(res.data.refresh_token)
          setJWTDecode(jwt(res.data.access_token))
          setShowUser(false)
          setuserData(null)
          setIsLogin(true)
        })
        .catch(err => {
          console.log("login Error")
      });
        }
        else{
            if(data.username === 'admin' && data.password === 'admin') setIsLogin(true);
            if(data.username === 'bob' && data.password === 'bob') setIsLogin(true);
            if(data.username === 'alice' && data.password === 'alice') setIsLogin(true);
            if(data.username === 'username' && data.password === 'password') setIsLogin(true);
        }
    }

    const encodeFunction = () => {
        console.log('Encode Complete')
        setShowJwt(!showJwt)
    }

    const FetchUserdata = () => {
        if(JWTToken && !showUser){
        axios.get(`http://localhost:5000/me`, { 
            'headers': { Authorization: `Bearer ${JWTToken}` }
        })
        .then(res => {
            setuserData(res.data)
            console.log(res.data)
            
          })
          .catch(err => {
            console.log("login Error")
            setuserData(null)
        });
        }

        setShowUser(!showUser)
    }

    const RefreshNewToken = () => {
        console.log('refresh New Token')
        axios.post(`http://localhost:5000/auth/refresh`, { 
            "token" : RefreshToken
        })
        .then(res => {
          setJWT(res.data.access_token)
          setRefresh(res.data.refresh_token)
          setJWTDecode(jwt(res.data.access_token))
          setShowUser(false)
          setuserData(null)
        })
        .catch(err => {
          console.log("Refresh Error")
      });
    }

    const LogoutKeepToken = () => {
        console.log('Logout Keep Token')
        setIsLogin(false)
        setuserData(null)
        setShowUser(false)
    }

    const LogoutClearToken = () => {
        console.log('Logout Clear Token')
        axios.post(`http://localhost:5000/auth/logout`, { 
            "token" : RefreshToken
        })
        .then(res => {
        })
        .catch(err => {
          console.log("logout Error")
      });

        setJWT('')
        setJWTDecode(null)
        setRefresh('')
        setIsLogin(false)
        setShowUser(false)
        setuserData(null)
    }

    const Header = () => (
        <CustomHeader>
            Token-Based Application Demo
        </CustomHeader>
    )

    const FormUserPassword = () => (
        <WrapperCenter>
        <CustomForm onSubmit={handleSubmit(handleonSubmit)}>
            <Row>
                <Col >
                    <WrapperFormControl>
                    <CustomFormLabel>Username</CustomFormLabel>
                    <CustomFormControl size="lg" type="user" placeholder="Enter username" 
                    {...register("username", {required: true})} />
                    </WrapperFormControl>
                </Col>
                <Col >
                    <WrapperFormControl>
                    <CustomFormLabel>Password</CustomFormLabel>
                    <CustomFormControl size="lg" type="password" placeholder="Enter password" 
                    {...register("password", {required: true})} />
                    </WrapperFormControl>
                </Col>
                <Col xs lg="3">
                    <WrapperButton>
                    <CustomButton type="submit" >Login</CustomButton>
                    </WrapperButton>
                </Col>
            </Row>
            <Row>
            <CustomButton style={{ fontSize: '17px', margin: '30px 0' }} 
                     onClick={() => FetchUserdata()}
                    >Fetch user Data</CustomButton>
            </Row>
        </CustomForm>
        </WrapperCenter>
    )

    const FormMenu = () => (
        <WrapperGrid>
            <CustomFormDiv>
            <Row >
            <CustomGreetText>
                Hi '{JWTDecode ? JWTDecode.sub : ''}'
            </CustomGreetText>
            </Row>
            <Row style={{ paddingTop: '20px', paddingBottom: '20px' }}>
                <Col >
                    <CustomButton style={{ fontSize: '17px', marginRight: '30px' }} 
                     onClick={() => FetchUserdata()}
                    >Fetch user Data</CustomButton>
                    <CustomButton style={{ fontSize: '17px', marginRight: '30px' }}
                     onClick={() => RefreshNewToken()}
                    >Refresh
                     new token</CustomButton>
                </Col>
                <Col style={{ display: 'contents'}}>
                    <CustomButton style={{ fontSize: '17px', color: 'black' ,
                                           backgroundColor: '#E9E9E9', marginRight: '30px' }}
                     onClick={() => LogoutKeepToken()}
                    >Logout,
                    but keep token</CustomButton>
                    <CustomButton style={{ fontSize: '17px', color: 'black' ,
                                           backgroundColor: '#E9E9E9', marginRight: '30px' }}
                     onClick={() => LogoutClearToken()}
                    >Logout, all client</CustomButton>
                </Col>
            </Row>
            </CustomFormDiv>
        </WrapperGrid>
    )

    const FormData = ({ 
        label,
        data,
        buttonLabel,
        formHeight,
        showExpire,
        showEncode
    }:{
        label: string,
        data: string,
        buttonLabel: string,
        formHeight: number,
        showExpire?: boolean
        showEncode?: any
    }) => {
        return(
        <WrapperCenter>
            <WrapperFormData>
            <Row>
                <Col xs lg="2">
                    <CustomText>
                    {label}
                    </CustomText>
                </Col>
                <Col>
                    <CustomFormTextArea
                    style={{ height: formHeight }}
                    as="textarea"
                    value= {data} 
                    readOnly />
                    { showExpire ? <p> expire in : {time} </p> : null }
                    { showEncode ?
                    <>
                        <CustomButtonGrey style={{marginBottom: '20px'}}
                        onClick={() => encodeFunction()}>
                            Show JWT Detail
                        </CustomButtonGrey>
                        <Toast show={showJwt} onClose={encodeFunction}>
                        <Toast.Header>
                            <strong className="me-auto">JWT Data</strong>
                        </Toast.Header>
                        <Toast.Body>{JSON.stringify(JWTDecode)}</Toast.Body>
                    </Toast>
                    </>
                        : null
                    }
                </Col>
                <Col xs lg="3">
                    <WrapperCenter>
                    <CustomButton type="submit" 
                    style={{ fontSize: '17px' }}
                    > {buttonLabel} </CustomButton>
                    </WrapperCenter>
                </Col>
            </Row>
            </WrapperFormData>
        </WrapperCenter>
    )}

    return(
        <div>
            <Header/>
            { !isLogin ? <FormUserPassword/> 
                : <FormMenu/>
            }    
            <WrapperCenter>
            <Toast show={showUser} onClose={FetchUserdata} style={{marginLeft: '-675px'}}>
                    <Toast.Header>
                        <strong className="me-auto">User Data</strong>
                    </Toast.Header>
                    <Toast.Body>{JSON.stringify(userData)}</Toast.Body>
                </Toast>
            </WrapperCenter>
            <WrapperCenter><LineUp/></WrapperCenter>
            <div>
            <FormData
                label= 'ClientID'
                data = {JWTDecode ? JWTDecode.sub : ''}
                buttonLabel= 'Overide Client ID'
                formHeight= {60}
            />
            <WrapperCenter><LightLineUp/></WrapperCenter>
            <FormData
                label= 'JWT Token'
                data = {JWTToken}
                buttonLabel= 'Overide JWT Token'
                formHeight= {180}
                showExpire= {true}
                showEncode= {encodeFunction}
            />
            <WrapperCenter><LightLineUp/></WrapperCenter>
            <FormData
                label= 'Refresh Token'
                data = {RefreshToken}
                buttonLabel= 'Overide Refresh Token'
                formHeight= {120}
            />
            <WrapperCenter><LightLineUp/></WrapperCenter>
            </div>
        </div>
    )
}

const CustomHeader = styled.div`
    font-size: 40px;
    font-weight: 500;
    text-align: center;
    margin: 30px 0;
`;

const WrapperCenter = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

const WrapperGrid = styled.div`
    display: grid;
    align-items: center;
    justify-content: center;
`;

const WrapperButton = styled.div`
    margin-top: 42px
`;

const WrapperFormControl = styled.div`
    padding-right: 30px;
`;

const CustomForm = styled(Form)`
    width: 1000px;
`;

const CustomFormDiv = styled.div`
    width: 1000px;
`;

const CustomButton = styled(Button)`
    width: 150px;
    height: 60px;
    font-size: 20px;
    font-weight: 500;
    background-color: #65AAEB;
    border: none;
    border-radius: 10px;
`;

const CustomFormControl = styled(Form.Control)`
    max-width: 400px;
    height: 60px;
    border-radius: 10px;
`;

const CustomFormLabel = styled(Form.Label)`
    font-size: 23px;
    font-weight: 500;
`;

const CustomGreetText = styled.div`
    font-size: 25px;
    font-weight: 400;
`;

const LineUp = styled.div`
    width: 1100px;
    height: 4px;
    background-color: #BCBCBC;
    margin: 50px 0;
`;

const CustomFormTextArea = styled(Form.Control)`
    border-radius: 10px;
    width: inherit;
    font-size: 18px;
    font-family: sans-serif;
    padding: 15px;
    background: #E5E5E5;
    border: none;
    resize: none;
`;

/////////////////////////////////////////////////////////////////

const WrapperFormData = styled.div`
    width: 900px;
`;

const CustomText = styled.div`
    font-size: 20px;
    font-weight: 500;
`;

const LightLineUp = styled.div`
    width: 900px;
    height: 3px;
    background-color: #E5E5E5;
    margin: 50px 0 ;
`;

const CustomButtonGrey = styled(Button)`
    color: black;
    width: 200px;
    height: 40px;
    font-size: 15px;
    font-weight: 400;
    background-color: #E9E9E9;
    border: none;
    border-radius: 10px;
`;

