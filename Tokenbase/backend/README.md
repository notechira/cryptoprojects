# Token-Based Application Demo API 
## <b>Development</b>
### Installation 
```
npm install
```
### How to start backend server
```
npm start 
```

### User for test
```
USER 1 :
    username: admin 
    password: admin
USER 2 :
    username: bob 
    password: bob
USER 3 : 
    username: alice 
    password: alice
USER 4 : 
    username: username
    password: password
```
