const express = require('express')
const router = express.Router()

const { generateToken } = require('../logic/generate_token')
const { validateUser, validateRefreshToken } = require('../logic/validate')
const { setterRefreshToken, removeRefershToken, getterRefershToken} = require('../logic/manage_refresh_token')
const { jwtDebugger } = require('../../middleware/debug_token')

const { v4: uuidv4 } = require('uuid');
const enumStatus = require('../../enum/http_status_code')
const enumError = require('../../enum/error_status')

router
.post('/login', async (req,res) => {
    var body = req.body

    let validate = await validateUser(body.username, body.password)

    if(!validate){
        return res.status(enumStatus.BAD_REQUEST).json({
            type : enumError.LOGIN_ERROR,
            msg : 'user unauthorization',
        })
    }
    
    await generateToken(body).then(async(result)=>{
        // save refersh token
        let uuidToken = uuidv4(result.refreshToken)
        await setterRefreshToken(uuidToken, result.refreshToken)

        /** START LOGGIN */
        console.log(`
=====================================================================

POST: /auth/login
    Credential matched!
    Logging in with 
        username: '${body.username}'
        password: '${body.password}'
        cliendId: '${body.username}'
    Returns tokens back to user: {
        access_token: '${result.accessToken}',
        refresh_token: '${uuidToken}'
    }

=====================================================================
        `)
        /** END LOGGIN */

        return res.status(enumStatus.OK).json({
            access_token: result.accessToken,
            refresh_token: uuidToken
        })
    }).catch((error) =>{
        console.log(error)
        return res.status(enumStatus.BAD_REQUEST).json({
            type : enumError.LOGIN_ERROR,
            msg : 'user login error',
        })
    })
})

.post('/refresh', async (req,res) => {
    const { token } = req.body;

    if(!token){
        return res.status(enumStatus.BAD_REQUEST).json({
            type : enumError.INVALID_REQ_BODY,
            msg : 'Cound not have token in request body',
        })
    }
    let validate = await validateRefreshToken(token)
    if(!validate){
        return res.status(enumStatus.BAD_REQUEST).json({
            type : enumError.INVALID_TOKEN,
            msg : 'Invalid refresh token',
        })
    }

    var refreshTokenRecord = await getterRefershToken()
    let decodedToken = jwtDebugger(refreshTokenRecord.get(token))
    var data = {
        username : decodedToken.sub
    }

    await generateToken(data).then(async(result)=>{
        await removeRefershToken(token)

        let uuidToken = uuidv4(result.refreshToken)
        await setterRefreshToken(uuidToken, result.refreshToken)

        /** START LOGGIN */
        console.log(`
POST: /auth/refresh
    DONE : Generating new token.

=====================================================================
        `)
        /** END LOGGIN */

        return res.status(enumStatus.OK).json({
            access_token: result.accessToken,
            refresh_token: uuidToken
        })
    }).catch((error) =>{
        console.log(error)
        return res.status(enumStatus.BAD_REQUEST).json({
            type : enumError.REFRESH_ERROR,
            msg : 'refresh token error',
        })
    })

})

.post('/logout', async (req,res) => {
    const { token } = req.body;

    if(!token){
        return res.status(enumStatus.BAD_REQUEST).json({
            type : enumError.INVALID_REQ_BODY,
            msg : 'Cound not have token in request body',
        })
    }
    var refreshTokenRecord = await getterRefershToken()
    let decodedToken = jwtDebugger(refreshTokenRecord.get(token))

    await removeRefershToken(token)

    /** START LOGGIN */
    console.log(`
POST: /auth/logout
    DONE : Deleting refresh token: ${token} from the memory.
    DONE : Deleting client: ${decodedToken.sub} from the memory.

=====================================================================
    `)
    /** END LOGGIN */

    return res.status(enumStatus.OK).json({
        status : true,
        msg : 'Logout successfully',
    })
})

module.exports = router