var refreshTokenRecorder = new Map()

const setterRefreshToken = async(uuid,token) => {
    refreshTokenRecorder.set(uuid, token)
}

const getterRefershToken = async() => {
    return refreshTokenRecorder
}

const removeRefershToken = async(uuid) => {
    return refreshTokenRecorder.delete(uuid)
}

module.exports = {
    setterRefreshToken,
    getterRefershToken,
    removeRefershToken
}