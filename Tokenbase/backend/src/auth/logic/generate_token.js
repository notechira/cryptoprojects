const jwt = require('jsonwebtoken')
const env = require('../../../config/config')


const generateToken = async(data) =>{
    var payload = {
        iss : 'tokenbase-auth-demo-server',
        sub : data.username
    }

    var accessToken = jwt.sign(payload, env.TOKEN_SECRET_KEY, {expiresIn: '5m'})
    var refreshToken = jwt.sign(payload, env.TOKEN_SECRET_KEY, {expiresIn: '60m'})

    return { accessToken , refreshToken }
}

module.exports = {
    generateToken
}