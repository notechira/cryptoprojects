const { getterWhitelist } = require('../../../config/whitelist_user')
const { getterRefershToken } = require('./manage_refresh_token')

const validateUser = async(username, password) => {
    var userWhitelist = await getterWhitelist()

    return userWhitelist.get(username) ? userWhitelist.get(username) == password ? true : false : false
}

const validateRefreshToken = async(uuidToken) => {
    var refreshTokenRecord = await getterRefershToken()

    return refreshTokenRecord.get(uuidToken) ? true : false
}

module.exports = {
    validateUser,
    validateRefreshToken
}