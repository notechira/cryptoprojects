const express = require('express')
const router = express.Router()

const { getterWhitelist } = require('../../../config/whitelist_user')
const { jwtDebugger } = require('../../middleware/debug_token')
const userData = require('../../../config/data')

const enumStatus = require('../../enum/http_status_code')

router
.get('', async (req,res) => {
    var decodedToken  = jwtDebugger(req.headers.authorization.split(' ')[1])
     
    let user = userData[decodedToken.sub]
    /** START LOGGIN */
    console.log(`
GET: /me
    DONE : Access token was verified.
    DONE : Fetching user data.
    Returns user's profile: {
        name: '${user.name}',
        surname: '${user.surname}',
        address: '${user.address}',
        email: '${user.email}'
    }

=====================================================================
    `)
    /** END LOGGIN */

    return res.status(enumStatus.OK).json({
        name: user.name,
        surname: user.surname,
        address: user.address,
        email: user.email
    })
})

module.exports = router