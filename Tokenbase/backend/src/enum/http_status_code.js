/**
 * HTTP STATUS CODE
 */
const OK                    = 200
const CREATED               = 201
const BAD_REQUEST           = 400
const UNAUTHORIZED          = 401
const SESSION_EXPIRED       = 403
const NOT_FOUND             = 404
const UNSUPPORTED_MEDIA     = 415

//SERVER OR SERVICES ERROR
const SERVER_ERROR          = 500
const SERVICE_UNAVAILABLE   = 503
const TIMEOUT               = 504
const PAGE_TOKEN_EXPIRED    = 463

module.exports = {
    OK,
    CREATED,
    BAD_REQUEST,
    UNAUTHORIZED,
    SESSION_EXPIRED,
    NOT_FOUND,
    UNSUPPORTED_MEDIA,
    SERVER_ERROR,
    SERVICE_UNAVAILABLE,
    TIMEOUT,
    PAGE_TOKEN_EXPIRED
}
