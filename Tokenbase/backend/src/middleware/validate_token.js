const jwt = require('jsonwebtoken')
const enumError = require('../enum/error_status')
const enumStatus = require('../enum/http_status_code')
const env = require('../../config/config')

const validateAuth = async (req,res,next) => {
    const authHeader = req.headers.authorization
    if (authHeader) {
        const token = authHeader.split(' ')[1]
        jwt.verify(token, env.TOKEN_SECRET_KEY, (err, user) => {
            if (err) {
                return res.status(enumStatus.SESSION_EXPIRED).json({
                    type : enumError.INVALID_TOKEN,
                    msg : 'Could not verify access token'
                })
            }
            req.user = user;
            next();
        });
    } else {
        return res.status(enumStatus.UNAUTHORIZED).json({
            type : enumError.REQUIRE_TOKEN,
            msg : 'Required access token in header'
        })
    }
}

module.exports = {
    validateAuth
}