const jwt = require('jsonwebtoken')
const env = require('../../config/config')

const jwtDebugger = (tokens) => {
    var decodedToken = jwt.verify(tokens, env.TOKEN_SECRET_KEY);
    return decodedToken
}

module.exports = {
    jwtDebugger
}