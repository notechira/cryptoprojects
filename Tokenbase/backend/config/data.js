module.exports = {
    admin : {
        name: 'Jirawit',
        surname: 'Sopa',
        address: 'Bangkok, Thailand',
        email: 'notez.admin@mail.com'
    },
    bob : {
        name: 'Bobby',
        surname: 'Boo',
        address: 'Bangkok, Thailand',
        email: 'bob.user@mail.com'
    },
    alice : {
        name: 'Alice',
        surname: 'Valencia',
        address: 'Bangkok, Thailand',
        email: 'alice.user@mail.com'
    },
    username : {
        name: 'Test-name',
        surname: 'Test-surname',
        address: 'Bangkok, Thailand',
        email: 'test.user@mail.com'
    },
}