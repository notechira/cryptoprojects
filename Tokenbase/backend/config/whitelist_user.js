var whitelistUser = new Map()

const initWhiteListUser = () => {
    whitelistUser.set('admin','admin')
                .set('bob','bob')
                .set('alice','alice')
                .set('username','password')
}

const getterWhitelist = async() =>{
    return whitelistUser
}

module.exports = {
    initWhiteListUser,
    getterWhitelist
}