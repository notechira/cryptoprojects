const express = require('express');
const bodyParser = require('body-parser');
const env = require('./config/config')
const cors = require('cors')
const app = express()

const authRouter = require('./src/auth/controller/auth')
const userRouter = require('./src/user/controller/user')

const { validateAuth } = require('./src/middleware/validate_token')
const { initWhiteListUser } = require('./config/whitelist_user')

const PORT = env.PORT

/** Initial Whitelist Test User */
initWhiteListUser()

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.set('view engine', 'ejs')

app.get("/check", async (req, res) => {
  res.status(200).json(`Server started`)
})

app.use(`/auth`, authRouter)
//app.use(validateAuth)
app.use(`/me`, userRouter)

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`)
});
